from simple_tests import ex

# 1
#
# Why write a function? Because we tell you. That is why.
# Naaw, you write functions because they are an invaluable asset to help you create better software. THAT is why!
#
# Rewrite this code into a reusable function.

import math

radius = 5
area = radius * radius * math.pi


# Your solution should like like this:

@ex(1)
def area(radius):
    # Your code here
    pass


# 2
#
# The cool thing is that a function can take loads of arguments.
#
# Write a function that takes speed as m/s (meter/second) and distance as m (meters) as arguments
# and returns the travel time in seconds
#
# The formula is:
#     distance = speed * time
@ex(2)
def time_to_travel(speed, dist):
    pass

# 3
#
# Now returning travel... time in seconds isn't all that useful.
# Please return a string with the travel time in the format:
# "h:m:s" where h obviously stands for the number of hours m for
# minutes and so on. Mark your solution with @ex(3) like in the
# previous exercises

# Your solution to #3


# 4
#
# Most who have ever travelled in a car know that car speed is measured in km/h
# please create another conversion formula that turns km/h into m/s for us. Thanks!
# Mark this with @ex(4).

# Your solution to #4


# 5
#
# Well done.
#
# Let's bring it all together.
#
# Write a function that returns formatted output for how long it will take to travel for instance,
# between Malmö and Helsingborg which happens to be 64 km. The driver is cautious and drives at an
# average speed of 85 km/h.
#
# Make sure the function works for different distance and speed data.

# Your solution to #5
